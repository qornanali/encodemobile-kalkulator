package encodemobile.kalkulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView texthasil;

    EditText angka1,angka2;

    Button btntambah,btnkurang,btnkali,btnbagi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalkulator);

        texthasil = (TextView) findViewById(R.id.texthasil);

        angka1 = (EditText) findViewById(R.id.angka1);
        angka2 = (EditText) findViewById(R.id.angka2);

        btntambah = (Button) findViewById(R.id.btntambah);
        btnkurang = (Button) findViewById(R.id.btnkurang);
        btnkali = (Button) findViewById(R.id.btnkali);
        btnbagi = (Button) findViewById(R.id.btnbagi);

    }

    private void kalkulasi(String btn){

        double a1,a2,hasil = 0;

        a1 = Double.parseDouble(angka1.getText().toString());
        a2 = Double.parseDouble(angka2.getText().toString());

        switch(btn){
            case "+" :
                hasil = a1 + a2;
                break;
            case "-" :
                hasil = a1 - a2;
                break;
            case "/" :
                hasil = a1 / a2;
                break;
            case "*" :
                hasil = a1 * a2;
                break;
        }

        texthasil.setText(hasil+"");

    }


    public void Ketikatambahdiklik(View view){
        kalkulasi("+");
    }

    public void Ketikakurangdiklik(View view){
        kalkulasi("-");
    }

    public void Ketikakalidiklik(View view){
        kalkulasi("*");
    }

    public void Ketikabagidiklik(View view){
        kalkulasi("/");
    }




}
